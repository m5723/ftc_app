package org.firstinspires.ftc.teamcode.Walbots7175.external.samples;

import org.firstinspires.ftc.teamcode.HardwareController;
import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousAction;


/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class AutonomousSample_Driving is an AutonomousAction that handles the driving of the robot.
 *
 * The speed of the wheels is defined by a vector2 and the duration of driving is dependent to
 * the property timeToWaitAfterFinished.
 */
public class AutonomousSample_Driving extends AutonomousAction
{

    private float[] vector2DrivingPower;    //Two floats for powering the driving motors

    /**
     * Initializes an instance of Autonomous_Driving
     *
     * @param hardware The hardware controller object of the robot to access and control it's hardware
     * @param name The name of the instance of the AutonomousAction (will be displayed on the phone)
     * @param vector2DrivingPower vector with 2 floats (leftPower, rightPower)
     */
    public AutonomousSample_Driving(HardwareController hardware, String name, float[] vector2DrivingPower)
    {
        super(hardware, name);
        this.vector2DrivingPower = vector2DrivingPower;
    }

    /**
     * The setup() method is called by the AutonomousController for setting up the
     * AutonomousAction preparing it for the run() method
     *
     * In this case there is nothing that has to be set up
     */
    @Override
    public void setup()
    {
        //Nothing to set up
    }

    /**
     * The run() method is called continually by the AutonomousController until the
     * function returns FINISHED (runs as long as it returns RUNNING)
     *
     * This method will set the power to all of the wheels and directly return FINISHED
     *
     * @return RUNNING, FINISHED, (nil=error)
     */
    @Override
    public ActionState run()
    {
        //Start driving
        //TODO: power motors to drive
//        hardware.powerLeftDrivingMotor(vector2DrivingPower[0]);
//        hardware.powerRightDrivingMotor(vector2DrivingPower[1]);

        return ActionState.FINISHED;
    }

    /**
     * The shutdown() method is called by the AutonomousController for finishing up all tasks and
     * getting to a state where the AutonomousAction is completed.
     *
     * In this case the motors moving the wheels are stopped so that the robot stops at the
     * destination.
     */
    @Override
    public void shutdown()
    {
        //Stop
        //TODO: Stop motors from driving
//        hardware.powerLeftDrivingMotor(0);
//        hardware.powerRightDrivingMotor(0);
    }
}
