package org.firstinspires.ftc.teamcode.Walbots7175.external.samples;

import org.firstinspires.ftc.teamcode.HardwareController;
import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousAction;


/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class Autonomous_ColorSensor is an AutonomousAction that handles the color sensor to get
 * information about which color the object has, that is in front of the sensor.
 */
public class AutonomousSample_ColorSensor extends AutonomousAction
{
    /**
     * The blueValue property indicates how much the object in front of the color sensor is blue
     */
    public int blueValue = 0;

    /**
     * Initializes an instance of Autonomous_ColorSensor
     *
     * @param hardware The hardware controller object of the robot to access and control it's hardware
     * @param name The name of the instance of the AutonomousAction (will be displayed on the phone)
     */
    public AutonomousSample_ColorSensor(HardwareController hardware, String name)
    {
        super(hardware, name);
    }

    /**
     * The setup() method is called by the AutonomousController for setting up the
     * AutonomousAction preparing it for the run() method
     *
     * In this case the LED of the color sensor is turned on
     */
    @Override
    public void setup()
    {
        //TODO: Set up the color sensor
//        hardware.turnColorSensorLEDOn(true);
    }

    /**
     * The run() method is called continually by the AutonomousController until the
     * function returns FINISHED (runs as long as it returns RUNNING)
     *
     * This method will check the color values of the color sensor and store the result in
     * blueValue.
     *
     * @return RUNNING, FINISHED, (nil=error)
     */
    @Override
    public ActionState run()
    {
        //TODO: Read color sensor value
//        blueValue = hardware.getColorSensorBlueValue();

        return ActionState.FINISHED;
    }

    /**
     * The shutdown() method is called by the AutonomousController for finishing up all tasks and
     * getting to a state where the AutonomousAction is completed.
     *
     * In this case the LED of the color sensor is turned off
     */
    @Override
    public void shutdown()
    {
        //TODO: Turn color sensor off
//        hardware.turnColorSensorLEDOn(false);
    }
}
